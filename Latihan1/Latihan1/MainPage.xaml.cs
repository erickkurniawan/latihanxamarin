﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Latihan1
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            btnHitung.Clicked += BtnHitung_Clicked;
            btnGridPage.Clicked += BtnGridPage_Clicked;
		}

        private void BtnGridPage_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SampleGridPage());
        }

        private void BtnHitung_Clicked(object sender, EventArgs e)
        {
            double alas = Convert.ToDouble(txtAlas.Text);
            double tinggi = Convert.ToDouble(txtTinggi.Text);
            double hasil = 0.5 * alas * tinggi;
            txtHasil.Text = hasil.ToString();
        }
    }
}
