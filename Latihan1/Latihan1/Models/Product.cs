﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Latihan1.Models
{
    public class Product
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string MyImage { get; set; }
    }


}
