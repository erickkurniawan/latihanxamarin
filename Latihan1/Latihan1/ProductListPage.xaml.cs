﻿using Latihan1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Latihan1
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProductListPage : ContentPage
    {
        public ProductListPage()
        {
            InitializeComponent();
            List<Product> listProduct = new List<Product>()
            {
                new Product{Name="Samsung Galaxy S9",
                    Description ="Deskrispsi dari Samsung Galaxy S9",
                    MyImage="s9.jpg",Quantity=5,Price=14000000},
                new Product{Name="Samsung Galaxy S8",
                    Description ="Deskrispsi dari Samsung Galaxy S8",
                    MyImage="s9.jpg",Quantity=8,Price=11000000},
                new Product{Name="Samsung Galaxy Note S9",
                    Description ="Deskrispsi dari Samsung Galaxy Note S9",
                    MyImage="s9.jpg",Quantity=2,Price=10000000},
                new Product{Name="Iphone X",
                    Description ="Deskrispsi dari Iphone X",
                    MyImage="iphonex.jpg",Quantity=5,Price=18000000}
            };
            productList.ItemsSource = listProduct;
            productList.ItemTapped += ProductList_ItemTapped;
            menuMain.Clicked += MenuMain_Clicked;
            menuSimple.Clicked += MenuSimple_Clicked;
        }

        private void MenuSimple_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SimpleListPage());
        }

        private void MenuMain_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MainPage());
        }

        private void ProductList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Product currProduct = (Product)e.Item;
            DisplayAlert("Keterangan", 
                "Anda memilih produk " + currProduct.Name, "OK");
        }
    }
}