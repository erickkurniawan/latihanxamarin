﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Latihan1
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SimpleListPage : ContentPage
	{
		public SimpleListPage ()
		{
			InitializeComponent();
            List<string> listProduk = new List<string>
            {
                "Samsung Galaxy S9","Samsung Galaxy Note S9","Iphone X", "IPhone 8",
                "Samsung Galaxy S8"
            };
            myListView.ItemsSource = listProduk;
            myListView.ItemTapped += MyListView_ItemTapped;
		}

        private void MyListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            string itemPilih = (string)e.Item;
            DisplayAlert("Keterangan", "Anda memilih: " + itemPilih, "OK");
        }
    }
}